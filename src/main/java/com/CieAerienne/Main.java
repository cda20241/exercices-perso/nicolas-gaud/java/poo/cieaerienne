package com.CieAerienne;

import com.CieAerienne.models.*;

import java.util.*;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Vol volAller = new Vol("123", "Paris", "Latino");
        Vol volRetour = new Vol("321", "Latino", "Paris");
        ArrayList<Vol> vols = new ArrayList<>();
        vols.add(volAller);
        vols.add(volRetour);

        Pilote pilote1 = new Pilote("Arthur", "Jean");
        Pilote pilote2 = new Pilote("Dupont", "Jean");
        Pilote pilote3 = new Pilote("Morin", "Jean");
        ArrayList<Pilote> pilotes = new ArrayList<>();
        pilotes.add(pilote1);
        pilotes.add(pilote2);
        pilotes.add(pilote3);

        Compagnie compagnie1 = new Compagnie("ABC");
        compagnie1.setPilotes(pilotes);
        Compagnie compagnie2 = new Compagnie("BCD");
        Compagnie compagnie3 = new Compagnie("CDE");
        ArrayList<Compagnie> compagnies = new ArrayList<>();
        compagnies.add(compagnie1);
        compagnies.add(compagnie2);
        compagnies.add(compagnie3);

        Pilote pilote = compagnies.get(0).getPilotes().get(0);
        pilote.setCompagnie(compagnies.get(0));

        Passager passager1 = new Passager("Moi", "Même");
        Passager passager2 = new Passager("Estruda", "Paolo");

        Siege siegeAller1 = new Siege(1, 2, 2);
        Siege siegeAller2 = new Siege(1, 3, 2);
        Siege siegeRetour1 = new Siege(2, 5, 2);
        ArrayList<Siege> sieges = new ArrayList<>();
        sieges.add(siegeAller1);
        sieges.add(siegeAller2);
        sieges.add(siegeRetour1);

        Avion avion = new Avion("Airplane");
        avion.setSieges(sieges);

        Billet billetAller1 = new Billet("123k5");
        passager1.acheterBillet(billetAller1.editerBillet(volAller, passager1, siegeAller1));
        Billet billetRetour1 = new Billet("321k5");
        passager1.acheterBillet(billetRetour1.editerBillet(volRetour, passager1, siegeRetour1));
        Billet billetAller2 = new Billet("123k6");
        passager1.acheterBillet(billetAller2.editerBillet(volAller, passager2, siegeAller2));


        volAller.initVol(pilote, avion, pilote.getCompagnie());
        passager1.voyager(volAller);
        passager2.voyager(volAller);
        pilote.piloter(pilote.getCompagnie(), volAller, avion);
        avion.voler(volAller, pilote);
        passager1.finirVoyager(billetAller1);
        passager2.finirVoyager(billetAller2);
        avion.atterir();
        volAller.finirVol();

        volRetour.initVol(pilote, avion, pilote.getCompagnie());
        passager1.voyager(volRetour);
        pilote.piloter(pilote.getCompagnie(), volRetour, avion);
        avion.voler(volRetour, pilote);
        passager1.finirVoyager(billetRetour1);
        avion.atterir();
        volRetour.finirVol();
    }
}