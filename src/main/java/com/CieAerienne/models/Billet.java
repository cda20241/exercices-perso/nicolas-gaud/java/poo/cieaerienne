package com.CieAerienne.models;

import java.util.Date;

public class Billet {

    private String numBillet;
    private Date dateReservation;
    private Date dateEmission;
    private Date datePaiement;
    private Siege siege;
    private Passager passager;
    private Vol vol;

    public Billet(String numBillet) {
        this.numBillet = numBillet;
    }

    public String getNumBillet() {
        return this.numBillet;
    }

    public void setNumBillet(String numBillet) {
        this.numBillet = numBillet;
    }

    public Date getDateReservation() {
        return this.dateReservation;
    }

    public void setDateReservation(Date dateReservation) {
        this.dateReservation = dateReservation;
    }

    public Date getDateEmission() {
        return this.dateEmission;
    }

    public void setDateEmission(Date dateEmission) {
        this.dateEmission = dateEmission;
    }

    public Date getDatePaiement() {
        return this.datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public Siege getSiege() {
        return this.siege;
    }

    public void setSiege(Siege siege) {
        this.siege = siege;
    }

    public Passager getPassager() {
        return this.passager;
    }

    public void setPassager(Passager passager) {
        this.passager = passager;
    }

    public Vol getVol() {
        return this.vol;
    }

    public void setVol(Vol vol) {
        this.vol = vol;
    }

    public Billet editerBillet(Vol vol, Passager passager, Siege siege){
        this.vol = vol;
        this.passager = passager;
        this.siege = siege;
        return this;
    }
}
