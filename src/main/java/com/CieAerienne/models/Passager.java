package com.CieAerienne.models;

import java.util.Date;
import java.util.List;

public class Passager {

    private String nom;
    private String prenom;
    private Date dateNaissance;
    private String telephone;
    private String status;
    private List<Billet> billets;
    private Vol vol;

    public Passager(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return this.dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Billet> getBillets() {
        return this.billets;
    }

    public void setBillets(List<Billet> billets) {
        this.billets = billets;
    }

    public Vol getVol() {
        return this.vol;
    }

    public void setVol(Vol vol) {
        this.vol = vol;
    }

    public void acheterBillet(Billet billet){
        this.billets.add(billet);
    }

    public void voyager(Vol vol){
        this.vol = vol;
    }

    public void finirVoyager(Billet billet){
        this.vol = null;
        this.billets.remove(billet);
    }
}
