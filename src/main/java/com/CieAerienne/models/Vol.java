package com.CieAerienne.models;

import java.sql.Time;
import java.util.Date;
import java.util.List;

public class Vol {

    private String numVol;
    private Date dateDepart;
    private Date dateRetour;
    private String heureDepart;
    private String heureArrivee;
    private String villeDepart;
    private String villeArrivee;
    private Time retard;
    private Compagnie compagnie;
    private Pilote pilote;
    private Avion avion;

    public Vol(String numVol, String villeDepart, String villeArrivee) {
        this.numVol = numVol;
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
    }

    public String getNumVol() {
        return this.numVol;
    }

    public void setNumVol(String numVol) {
        this.numVol = numVol;
    }

    public Date getDateDepart() {
        return this.dateDepart;
    }

    public void setDateDepart(Date dateDepart) {
        this.dateDepart = dateDepart;
    }

    public Date getDateRetour() {
        return this.dateRetour;
    }

    public void setDateRetour(Date dateRetour) {
        this.dateRetour = dateRetour;
    }

    public String getHeureDepart() {
        return this.heureDepart;
    }

    public void setHeureDepart(String heureDepart) {
        this.heureDepart = heureDepart;
    }

    public String getHeureArrivee() {
        return this.heureArrivee;
    }

    public void setHeureArrivee(String heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public String getVilleDepart() {
        return this.villeDepart;
    }

    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }

    public String getVilleArrivee() {
        return this.villeArrivee;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public Time getRetard() {
        return this.retard;
    }

    public void setRetard(Time retard) {
        this.retard = retard;
    }

    public Compagnie getCompagnie() {
        return this.compagnie;
    }

    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }

    public Pilote getPilote() {
        return this.pilote;
    }

    public void setPilote(Pilote pilote) {
        this.pilote = pilote;
    }

    public Avion getAvion() {
        return this.avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public void initVol(Pilote pilote, Avion avion, Compagnie compagnie){
        this.pilote = pilote;
        this.avion = avion;
        this.compagnie = compagnie;
    }

    public void finirVol(){
        this.pilote = null;
        this.avion = null;
        this.compagnie = null;
    }
}
