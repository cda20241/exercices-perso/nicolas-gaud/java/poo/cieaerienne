package com.CieAerienne.models;

import java.util.List;

public class Compagnie {
    private int code;
    private String nom;
    private String siegeSocial;
    private List<Pilote> pilotes;
    private List<Vol> vols;

    public Compagnie(String nom) {
        this.nom = nom;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSiegeSocial() {
        return this.siegeSocial;
    }

    public void setSiegeSocial(String siegeSocial) {
        this.siegeSocial = siegeSocial;
    }

    public List<Pilote> getPilotes() {
        return this.pilotes;
    }

    public void setPilotes(List<Pilote> pilotes) {
        this.pilotes = pilotes;
    }

    public List<Vol> getVols() {
        return vols;
    }

    public void setVols(List<Vol> vols) {
        this.vols = vols;
    }

    public void embaucherPilote(Pilote pilote){
        this.pilotes.add(pilote);
    }

    public void licenciererPilote(Pilote pilote){
        this.pilotes.remove(pilote);
    }

    public void ajouterVol(Vol vol){
        this.vols.add(vol);
    }

    public void supprimerVol(Vol vol){
        this.vols.remove(vol);
    }
}
