package com.CieAerienne.models;

public class Siege {

    private int numAllee;
    private int numRang;
    private int classe;
    private Avion avion;
    private Billet billet;

    public Siege(int numAllee, int numRang, int classe) {
        this.numAllee = numAllee;
        this.numRang = numRang;
        this.classe = classe;
    }

    public int getNumAllee() {
        return this.numAllee;
    }

    public void setNumAllee(int numAllee) {
        this.numAllee = numAllee;
    }

    public int getNumRang() {
        return this.numRang;
    }

    public void setNumRang(int numRang) {
        this.numRang = numRang;
    }

    public int getClasse() {
        return this.classe;
    }

    public void setClasse(int classe) {
        this.classe = classe;
    }

    public Avion getAvion() {
        return this.avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public Billet getBillet() {
        return billet;
    }

    public void setBillet(Billet billet) {
        this.billet = billet;
    }

    public void attribuerSiege(Avion avion, Billet billet){
        this.avion = avion;
        this.billet = billet;
    }

    public void librererSiege(){
        this.avion = null;
        this.billet = null;
    }
}
