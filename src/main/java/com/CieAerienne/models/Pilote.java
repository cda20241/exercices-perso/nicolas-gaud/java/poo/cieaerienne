package com.CieAerienne.models;

import java.util.List;

public class Pilote {

    private int matricule;
    private String nom;
    private String prenom;
    private String qualif;
    private Compagnie compagnie;
    private Vol vol;


    public Pilote(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public int getMatricule() {
        return matricule;
    }

    public void setMatricule(int matricule) {
        this.matricule = matricule;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getQualif() {
        return this.qualif;
    }

    public void setQualif(String qualif) {
        this.qualif = qualif;
    }


    public Compagnie getCompagnie() {
        return this.compagnie;
    }

    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }

    public Vol getVol() {
        return this.vol;
    }

    public void setVol(Vol vol) {
        this.vol = vol;
    }

    public void piloter(Compagnie compagnie, Vol vol, Avion avion){
        this.vol = vol;
        this.compagnie = compagnie;
        this.vol.initVol(this, avion, this.getCompagnie());
    }

    public void finirPiloter(){
        this.vol.finirVol();
        this.vol = null;
    }

    public void etreLicencie(Compagnie compagnie){
        this.compagnie = null;
    }
}
