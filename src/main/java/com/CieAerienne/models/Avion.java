package com.CieAerienne.models;

import java.util.List;

public class Avion {

    private int codeAvion;
    private String typeAvion;
    private String modeleAvion;
    private int nbPassagers;
    private Vol vol;
    private Pilote pilote;
    private List<Siege> sieges;

    public Avion(String modeleAvion) {
        this.modeleAvion = modeleAvion;
    }

    public int getCodeAvion() {
        return this.codeAvion;
    }

    public void setCodeAvion(int codeAvion) {
        this.codeAvion = codeAvion;
    }

    public String getTypeAvion() {
        return this.typeAvion;
    }

    public void setTypeAvion(String typeAvion) {
        this.typeAvion = typeAvion;
    }

    public String getModeleAvion() {
        return this.modeleAvion;
    }

    public void setModeleAvion(String modeleAvion) {
        this.modeleAvion = modeleAvion;
    }

    public int getNbPassagers() {
        return this.nbPassagers;
    }

    public void setNbPassagers(int nbPassagers) {
        this.nbPassagers = nbPassagers;
    }

    public Vol getVol() {
        return this.vol;
    }

    public void setVol(Vol vol) {
        this.vol = vol;
    }

    public Pilote getPilote() {
        return this.pilote;
    }

    public void setPilote(Pilote pilote) {
        this.pilote = pilote;
    }

    public List<Siege> getSieges() {
        return this.sieges;
    }

    public void setSieges(List<Siege> sieges) {
        this.sieges = sieges;
    }

    public void voler(Vol vol, Pilote pilote){
        this.vol = vol;
        this.pilote = pilote;
    }

    public void atterir(){
        this.vol = null;
        this.pilote = null;
    }
}
